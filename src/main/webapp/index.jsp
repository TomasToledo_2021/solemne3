<%-- 
    Document   : index
    Created on : 04-07-2021, 13:31:15
    Author     : Tomas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Solemne 3</title>
        <link type="text/css" rel="stylesheet" href="css/estilos.css" media="screen" /></link>
    </head>
    <body>
        <form name="frmRegistro" id="frmRegistro" action="RegistroController" method="POST">
            <table border="0" cellspacing="0" cellpadding="5" align="center" width="600" height="300">
                <tr>
                    <td align="center">
                        <fieldset>
                            <legend>Solemne 3</legend>
                            <table class="table extBorder" align="center" cellpadding="2" cellspacing="1" border="0" width="600">
                                <tr>
                                    <td align="right" class="tituloItem">
                                        Listar:
                                    </td>
                                    <td>
                                        <a href="https://solemne3-toledo.herokuapp.com/api/reciclaje" target="_blank">https://solemne3-toledo.herokuapp.com/</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="tituloItem">
                                        Crear:
                                    </td>
                                    <td>
                                        <a href="https://solemne3-toledo.herokuapp.com/api/reciclaje" target="_blank">https://solemne3-toledo.herokuapp.com/</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="tituloItem">
                                        Editar:
                                    </td>
                                    <td>
                                        <a href="https://solemne3-toledo.herokuapp.com/api/reciclaje" target="_blank">https://solemne3-toledo.herokuapp.com/</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="tituloItem">
                                        Buscar:
                                    </td>
                                    <td>
                                        <a href="https://solemne3-toledo.herokuapp.com/api/reciclaje/00120" target="_blank">https://solemne3-toledo.herokuapp.com/00120</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="tituloItem">
                                        Eliminar:
                                    </td>
                                    <td>
                                        <a href="https://solemne3-toledo.herokuapp.com/api/reciclaje/10521" target="_blank">https://solemne3-toledo.herokuapp.com/10521</a>
                                    </td>
                                </tr>
                            </table>
                            <br>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
