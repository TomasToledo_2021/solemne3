/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tomastoledo.solemne3.tae.services;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import tomastoledo.solemne3.tae.dao.ReciclajeJpaController;
import tomastoledo.solemne3.tae.dao.exceptions.NonexistentEntityException;
import tomastoledo.solemne3.tae.entity.Reciclaje;

/**
 *
 * @author Tomas
 */
@Path("reciclaje")
public class ReciclajeRest {
   @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarLote() {
        ReciclajeJpaController daoReciclaje = new ReciclajeJpaController();
        List<Reciclaje> listarLote = daoReciclaje.findReciclajeEntities();
        return Response.ok(200).entity(listarLote).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearLote(Reciclaje datosReciclaje) {
        ReciclajeJpaController daoReciclaje = new ReciclajeJpaController();
        try {
            daoReciclaje.create(datosReciclaje);
        } catch (Exception ex) {
            Logger.getLogger(ReciclajeRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(datosReciclaje).build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarLote(Reciclaje datosReciclaje) {
        ReciclajeJpaController daoReciclaje = new ReciclajeJpaController();
        try {
            daoReciclaje.edit(datosReciclaje);
        } catch (Exception ex) {
            Logger.getLogger(ReciclajeRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(datosReciclaje).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{ID_Lote}")//recibe rut
    public Response buscarLote(@PathParam("ID_Lote") int ID_Lote) {
        ReciclajeJpaController daoReciclaje = new ReciclajeJpaController();
        Reciclaje datosReciclaje = daoReciclaje.findReciclaje(ID_Lote);
        return Response.ok(200).entity(datosReciclaje).build();
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{ID_Lote}")
    public Response eliminarLote(@PathParam("ID_Lote") int ID_Lote) {
        ReciclajeJpaController daoReciclaje = new ReciclajeJpaController();
        try {
            daoReciclaje.destroy(ID_Lote);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(ReciclajeRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity("{\"Atencion \":\"registro eliminado \"}").build();
    }
}
