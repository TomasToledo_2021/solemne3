/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tomastoledo.solemne3.tae.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Tomas
 */
@Entity
@Table(name = "reciclaje")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reciclaje.findAll", query = "SELECT r FROM Reciclaje r"),
    @NamedQuery(name = "Reciclaje.findByIDLote", query = "SELECT r FROM Reciclaje r WHERE r.iDLote = :iDLote"),
    @NamedQuery(name = "Reciclaje.findByMaterial", query = "SELECT r FROM Reciclaje r WHERE r.material = :material"),
    @NamedQuery(name = "Reciclaje.findByCantidad", query = "SELECT r FROM Reciclaje r WHERE r.cantidad = :cantidad"),
    @NamedQuery(name = "Reciclaje.findByFecha", query = "SELECT r FROM Reciclaje r WHERE r.fecha = :fecha"),
    @NamedQuery(name = "Reciclaje.findByEncargado", query = "SELECT r FROM Reciclaje r WHERE r.encargado = :encargado")})
public class Reciclaje implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_Lote")
    private Integer iDLote;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "Material")
    private String material;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Cantidad")
    private int cantidad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "Encargado")
    private String encargado;

    public Reciclaje() {
    }

    public Reciclaje(Integer iDLote) {
        this.iDLote = iDLote;
    }

    public Reciclaje(Integer iDLote, String material, int cantidad, Date fecha, String encargado) {
        this.iDLote = iDLote;
        this.material = material;
        this.cantidad = cantidad;
        this.fecha = fecha;
        this.encargado = encargado;
    }

    public Integer getIDLote() {
        return iDLote;
    }

    public void setIDLote(Integer iDLote) {
        this.iDLote = iDLote;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getEncargado() {
        return encargado;
    }

    public void setEncargado(String encargado) {
        this.encargado = encargado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iDLote != null ? iDLote.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reciclaje)) {
            return false;
        }
        Reciclaje other = (Reciclaje) object;
        if ((this.iDLote == null && other.iDLote != null) || (this.iDLote != null && !this.iDLote.equals(other.iDLote))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "tomastoledo.solemne3.tae.entity.Reciclaje[ iDLote=" + iDLote + " ]";
    }
    
}
