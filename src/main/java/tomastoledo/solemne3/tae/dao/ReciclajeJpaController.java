/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tomastoledo.solemne3.tae.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import tomastoledo.solemne3.tae.dao.exceptions.NonexistentEntityException;
import tomastoledo.solemne3.tae.dao.exceptions.PreexistingEntityException;
import tomastoledo.solemne3.tae.entity.Reciclaje;

/**
 *
 * @author Tomas
 */
public class ReciclajeJpaController implements Serializable {

    public ReciclajeJpaController() {
       // this.emf = emf;
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("UP_SOLEMNE_3");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Reciclaje reciclaje) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(reciclaje);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findReciclaje(reciclaje.getIDLote()) != null) {
                throw new PreexistingEntityException("Reciclaje " + reciclaje + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Reciclaje reciclaje) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            reciclaje = em.merge(reciclaje);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = reciclaje.getIDLote();
                if (findReciclaje(id) == null) {
                    throw new NonexistentEntityException("The reciclaje with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Reciclaje reciclaje;
            try {
                reciclaje = em.getReference(Reciclaje.class, id);
                reciclaje.getIDLote();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The reciclaje with id " + id + " no longer exists.", enfe);
            }
            em.remove(reciclaje);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Reciclaje> findReciclajeEntities() {
        return findReciclajeEntities(true, -1, -1);
    }

    public List<Reciclaje> findReciclajeEntities(int maxResults, int firstResult) {
        return findReciclajeEntities(false, maxResults, firstResult);
    }

    private List<Reciclaje> findReciclajeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Reciclaje.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Reciclaje findReciclaje(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Reciclaje.class, id);
        } finally {
            em.close();
        }
    }

    public int getReciclajeCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Reciclaje> rt = cq.from(Reciclaje.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
