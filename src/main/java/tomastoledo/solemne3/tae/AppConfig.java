package tomastoledo.solemne3.tae;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Configures JAX-RS for the application.
 * @author Tomas
 */
@ApplicationPath("api")
public class AppConfig extends Application {
    
}
